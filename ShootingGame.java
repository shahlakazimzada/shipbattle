import java.util.Random;
import java.util.Scanner;

public class ShootingGame
{
    public static void main(String[] args) {


        char[][] field = new char[5][5];
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                field[i][j] = '-';
            }
        }

        Random random = new Random();
        int targetRow = random.nextInt(5);
        int targetColumn = random.nextInt(5);

        System.out.println("All set. Get ready to rumble!");

        Scanner sc = new Scanner(System.in);

        while (true)
        {
            int shootRow = getValidInput(sc, "Enter row(1-5): ");
            int shootColumn = getValidInput(sc, "Enter column(1-5): ");

            if(shootRow == targetRow && shootColumn == targetColumn)
            {
                field[shootRow][shootColumn] = 'x';
                printField(field);
                System.out.println("You have won!");
                break;

          }

            else
            {
                field[shootRow][shootColumn] = '*';
                printField(field);
            }

        }

        sc.close();
    }

    public static int getValidInput(Scanner sc , String prompt)
    {
        int value = 0;
        while (true)
        {
            System.out.print(prompt);
            if(sc.hasNextInt())
            {
                value = sc.nextInt();
                if( value >= 1 && value <= 5)
                {
                    break;
                }
                else
                {
                    System.out.println("Invalid input Please enter a number between 1 and 5.");
                }

            }
            else
            {
                System.out.println("Invalid input. Please enter a valid integer.");
                sc.next();
            }
        }
        return value;
    }


    public static void printField(char[][] field)
    {
        System.out.println("  0 | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 0; i < 5; i++)
        {
            System.out.print(" " + (i + 1) + " | ");
            for (int j = 0; j < 5; j++)
            {
                System.out.print(field[i][j] + " | ");
            }
            System.out.println();
        }


    }

}
